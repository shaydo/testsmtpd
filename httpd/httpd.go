package httpd

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
)

type Server struct {
	h *handler
	s *http.Server
}

func New(addr string) *Server {
	h := newHandler()
	mux := http.NewServeMux()
	mux.Handle("/wait", h)
	s := &http.Server{
		Addr:    addr,
		Handler: mux,
	}
	go s.ListenAndServe()
	return &Server{h: h, s: s}
}

func (s *Server) PushMessage(email string, msg interface{}) {
	s.h.pushMessage(email, msg)
}

type handler struct {
	mu sync.RWMutex
	q  map[string]chan<- interface{}
}

func newHandler() *handler {
	h := &handler{
		q: make(map[string]chan<- interface{}),
	}
	return h
}

func (h *handler) addWatcher(email string, c chan<- interface{}) error {
	h.mu.Lock()
	defer h.mu.Unlock()
	if _, ok := h.q[email]; ok {
		return fmt.Errorf("email is being watched already")
	}
	h.q[email] = c
	return nil
}

func (h *handler) delWatcher(email string) {
	h.mu.Lock()
	delete(h.q, email)
	h.mu.Unlock()
}

func (h *handler) pushMessage(email string, msg interface{}) {
	h.mu.Lock()
	c, ok := h.q[email]
	if ok {
		delete(h.q, email)
	}
	h.mu.Unlock()
	if ok {
		c <- msg
		close(c)
	}
}

func (h *handler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	c := make(chan interface{})
	ctx := r.Context()
	email := r.URL.Query().Get("email")
	if email == "" {
		http.Error(rw, "email is not specified", 400)
		return
	}
	if err := h.addWatcher(email, c); err != nil {
		http.Error(rw, "couldn't add a watcher", 500)
	}
	select {
	case <-ctx.Done():
		h.delWatcher(email)
	case m := <-c:
		enc := json.NewEncoder(rw)
		rw.Header().Set("Content-Type", "application/json")
		if err := enc.Encode(m); err != nil {
			http.Error(rw, "couldn't encode the mail", 500)
		}
	}
}
