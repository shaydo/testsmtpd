FROM golang:alpine AS build
COPY . /workspace
WORKDIR /workspace
RUN go build -o /bin/testsmtpd ./cmd

FROM alpine
COPY --from=build /bin/testsmtpd /bin/testsmtpd
ENTRYPOINT ["/bin/testsmtpd"]
