package smtpd

import (
	"io"
	"io/ioutil"

	"github.com/emersion/go-smtp"
)

type Message struct {
	From string   `json:"from"`
	To   []string `json:"to"`
	Data []byte   `json:"data"`
}

type SMTPD struct {
	c chan Message
	s *smtp.Server
}

func New(addr string) (*SMTPD, error) {
	c := make(chan Message)
	h := &handler{c: c}
	s := smtp.NewServer(h)
	s.Addr = addr
	s.Domain = "localhost"
	s.AllowInsecureAuth = true
	go s.ListenAndServe()
	return &SMTPD{c: c, s: s}, nil
}

func (d *SMTPD) Messages() <-chan Message {
	return d.c
}

func (d *SMTPD) Close() error {
	d.s.Close()
	close(d.c)
	return nil
}

type handler struct {
	c chan Message
}

func (h *handler) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	return &session{c: h.c}, nil
}

func (h *handler) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	return &session{c: h.c}, nil
}

type session struct {
	c chan<- Message
	m Message
}

func (s *session) Reset() {
	s.m = Message{}
}

func (s *session) Logout() error {
	return nil
}

func (s *session) Mail(from string, opts smtp.MailOptions) error {
	s.m.From = from
	return nil
}

func (s *session) Rcpt(to string) error {
	s.m.To = append(s.m.To, to)
	return nil
}

func (s *session) Data(r io.Reader) error {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	s.m.Data = b
	s.c <- s.m
	s.m = Message{}
	return nil
}
