package main

import (
	"gitlab.com/shaydo/testsmtpd/httpd"
	"gitlab.com/shaydo/testsmtpd/smtpd"
)

func main() {
	sd, err := smtpd.New(":25")
	if err != nil {
		panic(err)
	}
	hd := httpd.New(":80")
	for msg := range sd.Messages() {
		for _, to := range msg.To {
			hd.PushMessage(to, msg)
		}
	}
}
